﻿using Castle.Components.DictionaryAdapter.Xml;
using DotnetHomeworkApi;
using DotnetHomeworkApi.Controllers;
using DotnetHomeworkApi.ReadContexts;
using DotnetHomeworkApi.ReadContexts.Enum;
using DotnetHomeworkApi.SaveContexts;
using DotnetHomeworkApi.SaveContexts.Enum;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Text.Json.Nodes;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace DotnetHomework.Tests
{
    public class Documents_Tests
    {
        [Fact]
        public async Task CheckXmlFromHdd()
        {

            ReadType readType = ReadType.xml;
            SaveType saveType = SaveType.hdd;
            var optionsBuilder = new DbContextOptionsBuilder<SaveContext>();
            optionsBuilder.UseMySql("Server=localhost;Port=3306;Database=docs;UserId=admin;Password=admin;", new MySqlServerVersion(new Version()));
            SaveContext context = new SaveContext(optionsBuilder.Options);
            var cont = new DocumentsController(context);
            var result_good =  await  cont.Get("string", saveType, readType)! as ContentResult;
            var result_bad =  await cont.Get("badId", saveType, readType)! as NotFoundResult;
            Assert.NotNull(result_good);
            Assert.Equal(200, result_good!.StatusCode);
            Assert.NotNull(result_bad);
            Assert.Equal(404, result_bad!.StatusCode);
            Assert.IsType<XDocument>(XDocument.Parse(result_good.Content!));
            Assert.NotNull(result_good.Content);
            Assert.Equal("application/xml", result_good.ContentType);
        }
        [Fact]
        public async Task CheckJsonFromHdd()
        {
            ReadType readType = ReadType.json;
            SaveType saveType = SaveType.hdd;
            var optionsBuilder = new DbContextOptionsBuilder<SaveContext>();
            optionsBuilder.UseMySql("Server=localhost;Port=3306;Database=docs;UserId=admin;Password=admin;", new MySqlServerVersion(new Version()));
            SaveContext context = new SaveContext(optionsBuilder.Options);
            var cont = new DocumentsController(context);
            var result_good = await cont.Get("string", saveType, readType) as ContentResult;
            var result_bad = await cont.Get("badId", saveType, readType) as NotFoundResult;
            Assert.NotNull(result_good);
            Assert.Equal(200, result_good!.StatusCode);
            Assert.NotNull(result_bad);
            Assert.Equal(404, result_bad!.StatusCode);
            Assert.IsType<JsonDocument>(JsonDocument.Parse(result_good.Content!));
            Assert.NotNull(result_good.Content);
            Assert.Equal("application/json", result_good.ContentType);
        }
        [Fact]
        public async Task CheckEditHdd ()
        {
            SaveType saveType = SaveType.hdd;
            var optionsBuilder = new DbContextOptionsBuilder<SaveContext>();
            optionsBuilder.UseMySql("Server=localhost;Port=3306;Database=docs;UserId=admin;Password=admin;", new MySqlServerVersion(new Version()));
            SaveContext context = new SaveContext(optionsBuilder.Options);
            var cont = new DocumentsController(context);
            var data = new JsonObject();
            data.Add("some", "data");
            data.Add("other", "dt");
            Document doc = new Document
            {
                id = "string",
                tags = new string[] { "interp", "done" },
                data = data
            };
            var result_good = await cont.Put("string", doc, saveType) as OkResult;
            var result_bad = await cont.Put("badId", doc, saveType) as BadRequestResult;
            Assert.Equal(200, result_good!.StatusCode);
            Assert.NotNull(result_good);
            Assert.Equal(400, result_bad!.StatusCode);
        }
        [Fact]
        public async Task CheckSaveHdd()
        {
            SaveType saveType = SaveType.hdd;
            var optionsBuilder = new DbContextOptionsBuilder<SaveContext>();
            optionsBuilder.UseMySql("Server=localhost;Port=3306;Database=docs;UserId=admin;Password=admin;", new MySqlServerVersion(new Version()));
            SaveContext context = new SaveContext(optionsBuilder.Options);
            var cont = new DocumentsController(context);
            var data = new JsonObject();
            data.Add("some", "data");
            data.Add("other", "dt");
            Document doc = new Document
            {
                id = "test",
                tags = new string[] { "interp", "done" },
                data = data
            };
            var result_good = await cont.Post(doc, saveType) as OkResult;
            var result_bad = await cont.Post(null, saveType) as BadRequestResult;
            Assert.Equal(200, result_good!.StatusCode);
            Assert.Equal(400, result_bad!.StatusCode);
        }

    }
}
