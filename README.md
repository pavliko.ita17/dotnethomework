# Dotnet Homework
Úkol DotnetHomework jsem vzal jako výzvu a dpolnil do něj i příklady rozšiřitelnosti, která byla hodně zmiňována v zadání. Děkuji za možnost si tento úkol vyzkoušet. Níže najdete popis rozšířeného rozhraní, které by nemělo rušit chod programu podle zadání. **Důležité je ovšem pustit databázi o které je řeč níže.**
## Dotnet Api
Api má kvůli své rozšiřitelnosti krom požadovaných parametrů ještě parametry volitelné. Pro volitelné parametry jsem zvolil Enum. Pro jeho správné puštění je kvůli migraci nutné spustit docker s databází v adresáři **\DotnetHomeworkApi\Docker**.
### Typy uložení
1. SaveType.**hdd** - defaultní nastavení - volání: https://localhost:5000/documents?saveType=0 nebo https://localhost:5000/documents
2. SaveType.**db** - uložení do databáze - volání: https://localhost:5000/documents?saveType=1
### Typy vyčítání
Pro vyčítání jsem defaultně zvolil uložiště **hdd**, pro každé vyčítání lze zvolit saveType a podle již dříve zmíněných hodnot program sám pozná odkud si má soubor vyčíst.
1. ReadType.**xml** - defaultní nastavení - https://localhost:5000/documents/{id}?readType=0 nebo https://localhost:5000/documents/{id}
2. ReadType.**json** - vyčte soubor v json formátu - https://localhost:5000/documents/{id}?readType=0

**Za {id} lze doplnit jakýkoliv identifikátor uloženého souboru**
