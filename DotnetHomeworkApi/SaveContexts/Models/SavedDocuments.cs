﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Nodes;

namespace DotnetHomeworkApi.SaveContexts.Models
{
    [Table("saveddocument")]
    [Index(nameof(Id))]
    [Index(nameof(Data))]
    public class SavedDocuments
    {
        [Key]
        [Required]
        public string Id { get; set; }
        [Column(TypeName ="json")]
        public string Data { get; set; }
    }
}
