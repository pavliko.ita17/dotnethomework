﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing.Constraints;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace DotnetHomeworkApi.SaveContexts
{
    public class SaveToFile : ISaveContext
    {
        private static readonly JsonSerializerOptions _options = new() { DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingNull };
        public string SaveDocument(Document document)
        {
            try
            {
                if(document != null)
                {
                    string fileName = @".\Files\" + document.id + ".json";
                    if (File.Exists(fileName))
                    {
                        File.Delete(fileName);
                    }
                    var json = JsonSerializer.SerializeToUtf8Bytes(document,_options);
                    File.WriteAllBytes(fileName, json);
                    return "ok";
                }
                else
                {
                    return "";
                }
            }
            catch(Exception)
            {
                return "";
            }
        }
        public void EditDocument(Document document,string id)
        {
            string fileName = @".\Files\" + id + ".json";
            string json = File.ReadAllText(fileName);
            string docJson = JsonSerializer.Serialize(document);
            if(!json.Equals(docJson))
            {
                var jsonSave = JsonSerializer.SerializeToUtf8Bytes(document, _options);
                File.WriteAllBytes(fileName, jsonSave);
            }
        }
        public string GetDocument(string id)
        {
            string fileName = @".\Files\" + id +".json";
            string json = File.ReadAllText(fileName);
            return  json;
        }
    }
}
