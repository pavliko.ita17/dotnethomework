﻿using DotnetHomeworkApi;
namespace DotnetHomeworkApi.SaveContexts
{
    public interface ISaveContext
    {
        string SaveDocument(Document document);
        void EditDocument(Document document,string id);
        string GetDocument(string id);
    }
}
