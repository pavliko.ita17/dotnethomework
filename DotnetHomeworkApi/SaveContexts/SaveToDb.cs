﻿using DotnetHomeworkApi.SaveContexts.Models;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace DotnetHomeworkApi.SaveContexts
{
    public class SaveToDb:ISaveContext
    {
        private readonly SaveContext _saveContext;
        public SaveToDb(SaveContext saveContext)
        {
            _saveContext = saveContext;
        }

        public string SaveDocument(Document document)
        {

            var json = JsonSerializer.Serialize(document);
            SavedDocuments doc = new SavedDocuments
            {
                Id = document.id!,
                Data = json
            };
            try
            {
                _saveContext.SavedDocuments.Add(doc);
                _saveContext.SaveChanges();
                return "ok";
            }
            catch (Exception)
            {
                return "";
            }

        }
        public void EditDocument(Document document, string id)
        {
            var documentInDb = _saveContext.SavedDocuments.Find(id);
            if (!documentInDb!.Data.Equals(document.data!.ToString()))
            {
                documentInDb.Data = document.data.ToString();
                _saveContext.SaveChanges();
            }
        }
        public string GetDocument(string id)
        {
            var document = _saveContext.SavedDocuments.Find(id);
            if(document != null)
            {
                return document.Data;
            }
            else
            {
                return "";
            }

        }
    }
}
