﻿using DotnetHomeworkApi.SaveContexts.Models;
using Microsoft.EntityFrameworkCore;
namespace DotnetHomeworkApi.SaveContexts
{
    public class SaveContext : DbContext
    {
        private ISaveContext? ISaveContext;

        public SaveContext( DbContextOptions options) : base(options)
        {

        }

        public void SetSave(ISaveContext saveContext)
        {
             ISaveContext = saveContext;
        }
        public string Savelogic(Document document)
        {
           string result =  ISaveContext!.SaveDocument(document);
           return result;
        }
        public void EditLogic(Document document,string id)
        {
            ISaveContext!.EditDocument(document,id);
        }
        public async Task<string> GetLogic(string id)
        {
            string document = ISaveContext!.GetDocument(id);
            return document;
        }
        public DbSet<SavedDocuments> SavedDocuments => Set<SavedDocuments>();
    }
}
