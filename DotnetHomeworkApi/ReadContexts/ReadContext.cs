﻿using DotnetHomeworkApi.ReadContexts.Enum;
using DotnetHomeworkApi.SaveContexts;
using DotnetHomeworkApi.SaveContexts.Enum;

namespace DotnetHomeworkApi.ReadContexts
{
    public class ReadContext
    {
        private IReadContext? _context;
        public ReadContext()
        {

        }
        public ReadContext(IReadContext context)
        {
            _context = context;
        }
        public void SetRead(IReadContext context)
        {
            _context = context;
        }
        public string ReadLogic(string document)
        {
           string doc=  _context!.ReturnDocument(document);
           return doc;
        }
    }
}
