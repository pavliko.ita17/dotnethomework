﻿using DotnetHomeworkApi.SaveContexts.Enum;
using DotnetHomeworkApi.SaveContexts;
using System.Xml;
using System.Text.Json.Serialization;
using Newtonsoft.Json;
using System.Xml.Linq;

namespace DotnetHomeworkApi.ReadContexts
{
    public class ReadXml:IReadContext
    {
        private static readonly XDeclaration _defaultDeclaration = new("1.0", null, null);
        public string ReturnDocument(string document)
        {
            var doc = JsonConvert.DeserializeXNode(document,"root")!;
            var declaration = doc.Declaration ?? _defaultDeclaration;
            return $"{declaration}{Environment.NewLine}{doc}";
        }
    }
}
