﻿using DotnetHomeworkApi.SaveContexts.Enum;
using System.Reflection.Metadata.Ecma335;

namespace DotnetHomeworkApi.ReadContexts
{
    public interface IReadContext
    {
        public string ReturnDocument(string document);
    }
}
