﻿namespace DotnetHomeworkApi.ReadContexts.Enum
{
    public enum ReadType
    {
        xml,
        json
    }
}
