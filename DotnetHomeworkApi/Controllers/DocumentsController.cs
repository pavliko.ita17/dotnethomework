﻿using DotnetHomeworkApi.ReadContexts.Enum;
using DotnetHomeworkApi.SaveContexts;
using DotnetHomeworkApi.SaveContexts.Enum;
using Microsoft.AspNetCore.Mvc;
using DotnetHomeworkApi.ReadContexts;
using System.Dynamic;
using System.Reflection.Metadata;
using System.Text.Json.Nodes;
using System.Net.Mime;
using System.Net;
using Microsoft.EntityFrameworkCore;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace DotnetHomeworkApi.Controllers
{
    [Route("documents")]
    [ApiController]
    [Consumes("application/json","application/xml")]
    [Produces("application/json","application/xml")]
    public class DocumentsController : ControllerBase
    {
        private readonly SaveContext _context;
        public DocumentsController(SaveContext context)
        {
            _context = context;
        }
        // GET api/<DocumentsController>/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(string id,SaveType saveType = SaveType.hdd,ReadType readType = ReadType.xml)
        {
            var ReadContext = new ReadContext();
            var contentRes = new ContentResult();
            var logic = new AppLogic(readType, saveType, _context, ReadContext);
            if(id != null)
            {
                contentRes = await logic.ReadLogic(id);
                if(contentRes.StatusCode == 404)
                {
                    return NotFound();
                }
                else
                {
                    return contentRes;
                }
            }
            return BadRequest();
            
        }

        // POST api/<DocumentsController>
        [HttpPost()]
        public async Task<IActionResult> Post([FromBody] Document document,SaveType saveType = SaveType.hdd)
        {
            var logic = new AppLogic(saveType, _context);
            string res;
            if(document != null)
            {
                res =  logic.SaveLogic(document);
                if (res.Equals("ok"))
                {
                    return Ok();
                }
                else
                {
                    return BadRequest();
                }
            }
            else
            {
                return BadRequest();
            }
        }

        // PUT api/<DocumentsController>/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(string id,[FromBody] Document document, SaveType saveType = SaveType.hdd)
        {
            var logic = new AppLogic(saveType, _context);
            string res;
            if(id !=null && document != null)
            {
                res = await logic.EditLogic(document,id);
                if (res.Equals("ok")) 
                { 
                    return Ok();
                }
                else
                {
                    return BadRequest();
                }
            }
            else
            {
                return BadRequest();
            }
        }

        // DELETE api/<DocumentsController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }

    }
}
