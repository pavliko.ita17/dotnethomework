﻿using DotnetHomeworkApi.ReadContexts;
using DotnetHomeworkApi.ReadContexts.Enum;
using DotnetHomeworkApi.SaveContexts;
using DotnetHomeworkApi.SaveContexts.Enum;
using Microsoft.AspNetCore.Mvc;

namespace DotnetHomeworkApi
{
    public class AppLogic
    {
        private ReadType? _readType;
        private SaveType? _saveType;
        private ReadContext? _readContext;
        private readonly SaveContext? _saveContext;
        public AppLogic(ReadType readType,SaveType saveType,SaveContext saveContext, ReadContext readContext)
        {
          _readType = readType;
          _saveType = saveType;
         _readContext = readContext;
         _saveContext = saveContext;
        }
        public AppLogic(SaveType saveType, SaveContext saveContext)
        {
            _saveType=saveType;
            _saveContext = saveContext;
        }
        public void ChooseLocation()
        {
            if(_saveType == SaveType.db)
            {
                _saveContext!.SetSave(new SaveToDb(_saveContext));
            }
            else
            {
                _saveContext!.SetSave(new SaveToFile());
            }
        }
        public string ChooseReadType()
        {
            string contentType = "";
            if (_readType == ReadType.json)
            {
                _readContext!.SetRead(new ReadJson());
                contentType = "application/json";
            }
            else
            {
                _readContext!.SetRead(new ReadXml());
                contentType = "application/xml";
            }
            return contentType;
        }
        public string SaveLogic(Document document)
        {
            string res = "";
            ChooseLocation();
            try
            {
                res = _saveContext!.Savelogic(document);
                return res;
            }
            catch
            {
                return res;
            }

        }
        public async Task<string> EditLogic(Document document, string id)
        {

            ChooseLocation();
            try
            {
                _saveContext!.EditLogic(document, id);
                return "ok";
            }
            catch
            {
                return "bad request";
            }

        }
        public async Task<ContentResult> ReadLogic(string id)
        {
            try
            {
                ChooseLocation();
                string document = await _saveContext!.GetLogic(id);
                string contentType = ChooseReadType();
                string documentInFormat = _readContext!.ReadLogic(document);
                if(documentInFormat != null)
                {
                    return new ContentResult
                    {
                        ContentType = contentType,
                        Content = documentInFormat,
                        StatusCode = 200
                    };
                }
                else
                {
                    return new ContentResult
                    {
                        StatusCode = 404
                    };
                }

            }
            catch
            {
                return new ContentResult { StatusCode = 404 };
            }

        }
    }
}
