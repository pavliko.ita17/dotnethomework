﻿using System.Text.Json.Nodes;

namespace DotnetHomeworkApi
{
    public class Document
    {
        public string? id { get; set; }
        public string[]? tags { get; set; }
        public JsonObject? data { get; set; }
    }
}
